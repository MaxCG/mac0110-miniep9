#Autores:
#
# Maximilian Cabrajac Goritz
# NUSP: 11795418
#
# Rodrigo Volpe Battistin
# NUSP: 11795464

function multiplica(a, b)
	dima = size(a)
	dimb = size(b)
	if dima[2] != dimb[1]
		return -1
	end

	c = zeros(dima[1], dimb[2])

	for i in 1:dima[1]
		for j in 1:dimb[2]
			for k in 1:dimb[2]
				c[i, j] += a[i, k] * b[k, j]
			end
		end
	end

	return c

end

function matrix_pot(M, p)
	if p < 1
		error("O expoente deve ser maior que 1")
	end
	if length(M) == 1
		M = [(M[1]) ^ p]
	else
		i = 1
		original = M
		while i < p
			M = multiplica(M, original)
			i += 1
		end
	end
	return M
end

function matrix_pot_by_squaring(M, p)
	
	if length(M) == 1
		return [M[1]^p]
	end

	if p == 1
		return M
	elseif p % 2 == 0
		M_sqrd = multiplica(M, M)
		return matrix_pot_by_squaring(M_sqrd, p/2)
	else p % 2 == 1
		M_sqrd = multiplica(M, M)
		resul = multiplica(M, matrix_pot_by_squaring(M_sqrd, (p-1)/2))
		return resul
	end
end

using LinearAlgebra

function compare_times()
	a = [30, 20, 35, 50]
	b = [10, 50000, 100000]

	for i in a
		for p in b
			m = Matrix(LinearAlgebra.I, i, i)
			print("SQR s=$(i) p=$(p)")
			@time matrix_pot_by_squaring(m, p)
			print("ITR s=$(i) p=$(p)")
			@time matrix_pot(m, p)
		end
	end
end
