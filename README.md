# MAC0110 - MiniEP9

Uma colaboração com Rodrigo Volpe Battistin
NUSP : 11795464

## Parte 1 - Função de Potenciação

```julia
matrix_pot(M, p)
```

Crie uma função que calcule a matriz M^p, assuma que p >= 1 e M é uma matriz quadrada e utilize a função multiplica(a, b) vista em aula.

## Parte 2 - Função de exponenciação por quadrados

```julia
matrix_pot_by_squaring(M, p)
```

Essa função tem a mesma funcionalidade da última, porém com uma grande otimização.
Este método funciona tanto com matrizes, como com números reais e é análago a transformar ``p`` em bits e calcular somente as potências relacionadas ao 1s.

Exemplo:

```
3^13

13 = 0b1101

3^8 * 3^4 * 3^1 = 3^13
```

E pode ser descrito da forma:

```
f(x, p) = f(1/x, -p); tal que p < 0
        = elemento neutro da multiplicacao; tal que p == 0
        = x; tal que p == 1
        = f(x^2, p/2); tal que p % 2 == 0
        = x * f(x^2, (p - 1)/2); tal que p % 2 == 1
```
