using Test
using LinearAlgebra

include("miniep9.jl")

function testmatrix_pot()
	@testset "matrix_pot()" begin
		m = [1 0; 0 1]
		@test matrix_pot(m, 5) == [1 0; 0 1]
		m = [1 2 3; 0 -1 -2; 1 4 1]
		@test matrix_pot(m, 3) == [6 4 -10; -2 3 8; -2 -14 -2]

		@test matrix_pot([1], 2) == [1]
	end
end

function testmatrix_pot_by_squaring()
	@testset "matrix_pot_by_squaring" begin
		a = [10, 20, 30]
		b = [5, 10, 15]

		for i in a
			for p in b
				m = Matrix(LinearAlgebra.I, i, i)
				@test matrix_pot_by_squaring(m[:, :], p) == m
			end
		end
	end
end

function test()
	testmatrix_pot()
	testmatrix_pot_by_squaring()
	compare_times()
end

test()
