# MiniEP9 - Parte 3 - Respostas
### Segue a tebela de tempos comparando as funções `matrix_pot` e `matrix_pot_by_squaring`
```
SQR s=30 p=10  0.000240 seconds (7 allocations: 28.969 KiB)
ITR s=30 p=10  0.022585 seconds (31.74 k allocations: 1.684 MiB)
SQR s=30 p=50000  0.001073 seconds (20 allocations: 143.750 KiB)
ITR s=30 p=50000  2.767902 seconds (50.00 k allocations: 350.945 MiB)
SQR s=30 p=100000  0.001044 seconds (21 allocations: 150.938 KiB)
ITR s=30 p=100000  5.632352 seconds (100.00 k allocations: 701.897 MiB)
SQR s=20 p=10  0.000064 seconds (4 allocations: 13.000 KiB)
ITR s=20 p=10  0.000149 seconds (9 allocations: 29.250 KiB)
SQR s=20 p=50000  0.000307 seconds (20 allocations: 65.000 KiB)
ITR s=20 p=50000  0.794135 seconds (50.00 k allocations: 158.688 MiB)
SQR s=20 p=100000  0.000316 seconds (21 allocations: 68.250 KiB)
ITR s=20 p=100000  1.593222 seconds (100.00 k allocations: 317.380 MiB)
SQR s=35 p=10  0.000359 seconds (4 allocations: 39.000 KiB)
ITR s=35 p=10  0.000807 seconds (9 allocations: 87.750 KiB)
SQR s=35 p=50000  0.001846 seconds (20 allocations: 195.000 KiB)
ITR s=35 p=50000  5.689309 seconds (50.00 k allocations: 476.065 MiB)
SQR s=35 p=100000  0.002190 seconds (21 allocations: 204.750 KiB)
ITR s=35 p=100000 11.679415 seconds (100.00 k allocations: 952.139 MiB)
SQR s=50 p=10  0.001134 seconds (8 allocations: 78.563 KiB)
ITR s=50 p=10  0.002802 seconds (18 allocations: 176.766 KiB)
SQR s=50 p=50000  0.006400 seconds (40 allocations: 392.813 KiB)
ITR s=50 p=50000 16.842443 seconds (100.00 k allocations: 958.996 MiB)
SQR s=50 p=100000  0.005934 seconds (42 allocations: 412.453 KiB)
ITR s=50 p=100000 34.681295 seconds (200.00 k allocations: 1.873 GiB)

Legenda: ITR = matrix_pot
         SQR = matrix_pot_by_squaring
         s = tamanho da matriz
         p = expoente
```
## Pergunta: Houve diferença nos tempos de execução? Por quê?
**Resposta**: Sim, a função "SQR" foi significativemente mais rápida, nos casos testados. Isso porque ela trabalha com o expoente a ser computado antes de realizar as multiplicações de matrizes em si, de maneira a diminuir a quantidade necessária dessas, resultando em uma drástica redução de complexidade.
